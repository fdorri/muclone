'''
Created on Sep 16, 2014

@author: jewellsean, A. Roth, A. Bouchard, fdorri
'''

from math import isinf, exp, log


def normalized_div_exp(a, b):

    if isinf(a):
        exp_a = 0
    else:
        exp_a = exp(a)

    if isinf(b):
        exp_b = 0
    else:
        exp_b = exp(b)
    
    return exp_a / (exp_a + exp_b)

    
def log_sum_exp(log_X):
    '''
    Given a list of values in log space, log_X. 
    
    Numerically safer than naive method.
    '''
    max_exp = max(log_X)
    
    if isinf(max_exp):
        return max_exp
    
    total = 0

    for x in log_X:
        total += exp(x - max_exp)
    
    return log(total) + max_exp

def isClose(x, y, TOL):
    if (abs(x - y) < TOL):
        return(True)
    else:
        return(False)