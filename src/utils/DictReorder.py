'''
Created on Sep 16, 2014

Annoying, but sometimes it is necessary to reorder a dict
Assume that it is 2-D, 

@author: jewellsean
'''

from collections import OrderedDict

'''
@param name1: the principle axis 
@param name2: the secondary axis

@return: an ordered dict with the axis switched 

'''
def switch_dict_axes(oldDict, name1, name2, name1_ids, name2_ids):
    out = OrderedDict()
    for primary in name2_ids:
        out[primary] = OrderedDict()
        for secondary in name1_ids:
            out[primary][secondary] = oldDict[secondary][primary]
    
    return out
        