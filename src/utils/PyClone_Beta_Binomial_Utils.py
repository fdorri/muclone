'''
Created on Sep 10, 2014

@author: jewellsean, fdorri 
'''

from __future__ import division

from math import exp, log
from pyc_data import pyc_beta_binomial
from pyc_data.pyc_beta_binomial import  PyCloneBetaBinomialDensity
import utils


def posterior_cluster_likelihood(cluster, data, precision_params, epsilon_mu, normal):
    
    prob = []
    prob_dic = {}
    for sample_id in data:
        tau_j = cluster[sample_id][1]
        phi = cluster[sample_id][0]
        dat = data[sample_id]
        likelihood = []
        tumour_content = dat.tumour_content
        for cn_n, cn_r, cn_v, mu_n, mu_r, mu_v, log_pi  in zip(dat.cn_n, dat.cn_r, dat.cn_v, dat.mu_n, dat.mu_r, dat.mu_v, dat.log_pi):
            temp = exp(log_pi) * exp (cluster_log_likelihood(
                                                            dat.b,
                                                            dat.d,
                                                            cn_n,
                                                            cn_r,
                                                            cn_v,
                                                            mu_n,
                                                            mu_r,
                                                            mu_v,
                                                            phi,
                                                            tumour_content,
                                                            precision_params, 
                                                            epsilon_mu,
                                                            normal))
            likelihood.append(temp)

        if sum(likelihood) != 0:
            prob.append(log(sum(likelihood)))
            prob_dic[sample_id] = log(sum(likelihood))
        else:
            prob.append(float("-inf"))
            prob_dic[sample_id] = float("-inf")
          

    prob_sum = sum(prob)
    priorLikelihoodVector = []
    priorLikelihoodVector.append(prob_sum)
    priorLikelihoodVector.append(log(tau_j))
    #***
    priorLikelihoodVector.append(prob_dic)
    #***
    return(priorLikelihoodVector)
        
            
    
    
def cluster_log_likelihood(b, d, cn_n, cn_r, cn_v, mu_n, mu_r, mu_v, cellular_frequency, tumour_content, precision_params, epsilon_mu, normal):
    mu = build_beta_binomial_parameters(b, d, cn_n, cn_r, cn_v, mu_n, mu_r, mu_v, cellular_frequency, tumour_content)
    
    
    param_a = mu * precision_params
    param_b = (1 - mu) * precision_params 
    return pyc_beta_binomial.log_beta_binomial_pdf(b, d, param_a, param_b)

def build_beta_binomial_parameters(b, d, cn_n, cn_r, cn_v, mu_n, mu_r, mu_v, cellular_frequency, tumour_content):
        f = cellular_frequency
        t = tumour_content

        p_n = (1 - t) * cn_n
        p_r = t * (1 - f) * cn_r
        p_v = t * f * cn_v
        
        norm_const = p_n + p_r + p_v
        
        p_n = p_n / norm_const
        p_r = p_r / norm_const
        p_v = p_v / norm_const
        
        mu = p_n * mu_n + p_r * mu_r + p_v * mu_v
        return(mu)
 
        
