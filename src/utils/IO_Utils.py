'''
Created on Sep 10, 2014

@author: fdorri, jewellsean

'''
import csv
import numpy as np
import pandas as pd
import os
from datetime import datetime
from collections import OrderedDict
from pyc_data import pyc_beta_binomial as py
from mutation_classifier.classification_engine import classification_engine


def read_mutation_trace(mcmc_trace):
    data = pd.read_csv(mcmc_trace, sep='\t')
    
    n = len(data.index)
    mcmcIter = np.arange(1, n + 1)

    data['mcmc'] = mcmcIter
    return data

def read_mutation_trace_2OrderedDict(mcmc_trace):
   
        data = OrderedDict()
              
        reader = csv.DictReader(open(mcmc_trace), delimiter='\t')
        header = reader.fieldnames
        mcmc_iter = 1
        for row in reader: 
            data[mcmc_iter] = OrderedDict()
            for mutant in header:
                data[mcmc_iter][mutant] = float(row[mutant])
            mcmc_iter = mcmc_iter + 1
        return data


def read_bare_trace_2OrderedDict(mcmc_trace):
    data = OrderedDict()
    reader = csv.DictReader(open(mcmc_trace), delimiter='\t', fieldnames=['univariate'])
    mcmc_iter = 1
    for row in reader: 
        data[mcmc_iter] = float(row['univariate'])
        mcmc_iter = mcmc_iter + 1
    return data

def create_results_folder(config):
    time_stamp = datetime.now().strftime('%Y-%m-%d-%H-%M-%S')
    output_path = os.path.join(config['working_dir'],config['trace_dir'], 'results', time_stamp)
    try:
        os.stat(output_path)
    except:
        os.makedirs(output_path) 
    return output_path

def create_trace_folder(config):
    time_stamp = datetime.now().strftime('%Y-%m-%d-%H-%M-%S')
    output_path = os.path.join(config['working_dir'],config['trace_dir'], 'trace',time_stamp)
    try:
        os.stat(output_path)
    except:
        os.makedirs(output_path) 
    return output_path

def read_flat_clusters(file_path):
    merged = pd.read_csv(file_path, delim_whitespace=True)
    cluster = range(0, len(merged))
    merged['cluster'] = cluster
    merged['mcmc'] = 1
    grouped = merged.groupby(['mcmc', 'cluster'])
    aggregate_mcmc = grouped.agg({'cluster':{'nElements':np.mean}, 'phi':{'avg' : np.mean}})
    aggregate_mcmc.loc[:, ('cluster','nElements')] = merged['prior'].values
    return aggregate_mcmc

def flat_from_clusters(data):
    merged = pd.DataFrame(data)
    cluster = range(0, len(merged))
    merged['cluster'] = cluster
    merged['mcmc'] = 1
    grouped = merged.groupby(['mcmc', 'cluster'])
    aggregate_mcmc = grouped.agg({'cluster':{'nElements':np.mean}, 'phi':{'avg' : np.mean}})
    aggregate_mcmc.loc[:, ('cluster','nElements')] = merged['prior'].values
    return aggregate_mcmc

def _form_sample_data(genData, error_rate, tumour_content):
    '''
    Load data from PyClone formatted input file.
    '''
    data = OrderedDict()
    nMutations = len(genData)
    
    for i in range(nMutations):
        mutation = py.load_mutation_from_dict(genData[i])

        data[mutation.id] = py._get_pyclone_data(mutation, error_rate, tumour_content)
    
    return data
    
def write_results(config, args, muclone_results):
    output_path = create_trace_folder(config)
    muclone_results['posterior'].to_csv(os.path.join(output_path, 'MuClone-posterior.tsv'), sep='\t')
    muclone_results['sample_specific'].to_csv(os.path.join(output_path, 'MuClone-sample-results.tsv'), sep='\t')
    muclone_results['cluster_label_info'].to_csv(os.path.join(output_path, 'MuClone-labels.tsv'), sep='\t', index=False, header=False)
    f = open(os.path.join(output_path, 'parameters.txt'), 'w')
    f.write('Working dir:' + config['working_dir'] + '\n')
    f.write('config_file:' + args.config_file + '\n')
    f.write('wildtype_prior:' + str(args.wildtype_prior) + '\n')
    f.write('precision' + str(args.precision) + '\n')
    f.write('phi_threshold:' + str(args.phi_threshold) + '\n')
    f.write('perturb_input:' + str(args.perturb_input) + '\n')
    f.write('remove_cluster:' + str(args.remove_cluster) + '\n')
    f.write('add_noise:' + str(args.add_noise) + '\n')
    f.close()

def write_options(args):
    save_dir = os.environ.get('SPECIFIED_RESULT_FOLDER')
    options_dir = os.path.join(save_dir, "executionInfo")
    if not os.path.exists(save_dir):
        os.makedirs(save_dir)
    if not os.path.exists(options_dir):
        os.makedirs(options_dir)
        
    DELIM = '\t'
    f = open(os.path.join(options_dir, 'options.map'), 'w')
    
    f.write('pos_num' +  DELIM + str(args.pos_num) + '\n')
    f.write('cluster_num' +  DELIM + str(args.cluster_num) + '\n')
    f.write('tumour_content' +  DELIM + str(args.tumour_content) + '\n')
    f.write('depth' + DELIM + str(args.depth) + '\n')
    f.write('max_copy' +  DELIM + str(args.max_copy) + '\n')
    f.write('wildtype_phi' + DELIM + str(args.wildtype_phi) + '\n')
    f.write('max_wildtype_number' + DELIM + str(args.max_wildtype_number) + '\n')
    f.write('error_rate' +  DELIM + str(args.error_rate) + '\n')
    f.write('seed' +  DELIM + str(args.seed) + '\n')
    f.write('phi_threshold' +  DELIM + str(args.phi_threshold) + '\n')
    f.write('threshold' +  DELIM + str(args.threshold) + '\n')
    f.write('wildtype_prior' +  DELIM + str(args.wildtype_prior) + '\n')
    f.write('wildtype_eps' +  DELIM + str(args.wildtype_eps) + '\n')
    f.write('precision' +  DELIM + str(args.precision) + '\n')
    f.write('remove_clusters' +  DELIM + str(args.remove_clusters) + '\n')
    f.write('noise_sd' +  DELIM + str(args.noise_sd) + '\n')

    
    
    
    f.close()
