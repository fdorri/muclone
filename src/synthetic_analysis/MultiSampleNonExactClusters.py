'''
Created on Feb 18, 2015
    
@author: jewellsean, fdorri
'''
from __future__ import division
import argparse
import math
import os

import pandas as pd
import numpy as np
from scipy.spatial import distance
from collections import OrderedDict
from pyc_data.pyc_config import get_mutation

from mutation_classifier.classification_engine import classification_engine
from pyc_data import Pyc_inputs
from pyc_data import Pyc_mcmc
from utils.IO_Utils import write_options
from simulation_data import data_simulation
from numpy.random import RandomState
from data_analysis import *


def generate_priors(data, args):
    mutations = OrderedDict()
    
    for sample in data.keys():
        mutations[sample] = []
        nMutations = len(data[sample]['normal_cn'])
        
        for i in range(nMutations):
            mutation_id = data[sample]['mutation_id'][i]
            
            ref_counts = int(data[sample]['ref_counts'][i])
            
            var_counts = int(data[sample]['var_counts'][i])
            
            normal_cn = int(data[sample]['normal_cn'][i])
            
            minor_cn = int(data[sample]['minor_cn'][i])
            
            major_cn = int(data[sample]['major_cn'][i])
            
            mutation = get_mutation(mutation_id,
                                    ref_counts,
                                    var_counts,
                                    normal_cn,
                                    minor_cn,
                                    major_cn,
                                    args.ref_prior,
                                    args.var_prior)
                                    
            mutations[sample].append(mutation.to_dict())
    return mutations
   

    
def remove_clusters(data, nClustersRemoved, random):
    sample_phi = data[1]
    tau = data[2]
    name = sample_phi.keys()
    nClusters = len(sample_phi[name[0]])
    
    if (nClustersRemoved >= (nClusters - 1)):
        raise RuntimeError("Cannot remove more clusters than exist." + str(nClusters) + " exist and " + str(nClustersRemoved) + " are trying to be removed.")

#this part randomly remove clusters while keeping ancestor cluster



################
    #the first cluster should be "1" and then from samller cluster to the wildtype
    index_keep = []
    index_remove = [i+1 for i in range(nClustersRemoved)]
    for ind in range(nClusters):
        if ind not in index_remove:
            index_keep.append(ind)

    for sample in sample_phi.keys():
        sample_phi[sample] = sample_phi[sample][index_keep]
    
    
    
    
    # remove cluster tau and renormalize
    removed_mass = 0
    for cluster in index_remove:
        removed_mass = removed_mass + tau[cluster]

    tau = np.asarray(tau)
    tau = tau[index_keep]
    for cluster in range(len(tau) - 1):
        tau[cluster] = tau[cluster] + removed_mass / (nClusters - nClustersRemoved - 1)

    return data[0], sample_phi, tau.tolist(), data[3]


def add_noise(data, sdNoiseAdded, random):
    sample_phi = data[1]
    name = sample_phi.keys()
    nClusters = len(sample_phi[name[0]])
    for sample in sample_phi.keys():
        for cluster in range(nClusters):
            if random.choice([True, False]):
                moved_pt = sample_phi[sample][cluster] + random.normal(loc=0.0, scale=sdNoiseAdded)
                if (moved_pt > 0 and moved_pt < 1):
                    sample_phi[sample][cluster] = moved_pt
                if (moved_pt > 1):
                    sample_phi[sample][cluster] = 1
            else:
                moved_pt = sample_phi[sample][cluster] - random.normal(loc=0.0, scale=sdNoiseAdded)
                if (moved_pt > 0 and moved_pt < 1):
                    sample_phi[sample][cluster] = moved_pt
                if (moved_pt < 0):
                    sample_phi[sample][cluster] = 0




    return data[0], sample_phi, data[2], data[3]


def log_range(aMin, bMax, incr):
    a = math.log10(aMin)
    b = math.log10(bMax)
    return np.arange(a, b, incr)


if __name__ == '__main__':
    
    ## Data simulation parameters
    
    parser = argparse.ArgumentParser(prog='MuCloneMultiSampleTesting')
    parser.add_argument('--hg_dir', help='''the reference genome directory''')
    parser.add_argument('--pos_num', type=int, help='''the number of positions''')
    parser.add_argument('--cluster_num', type=int, help='''the number of clusters''')
    parser.add_argument('--tumour_content', type=float, help='''tumour_content''')
    parser.add_argument('--depth', type=int, help='''average depth of the data''')
    parser.add_argument('--max_copy', type=int, help='''max copy number''')
    parser.add_argument('--wildtype_phi', type=float, default=0, help='''mu for wildtype cluster''')
    parser.add_argument('--phi_threshold', type=float, default=0, help='''cutoff phi for classification''')
    parser.add_argument('--max_wildtype_number', type=int, help='''number of wildtype position in simulated data''')
    parser.add_argument('--precision', type=int, default=10, help='''precision parameter for beta-binomial''')
    parser.add_argument('--error_rate', type=float, help='''error rate for accounting for sequencing error''')
    parser.add_argument('--seed', type=int, default=1)
    parser.add_argument('--threshold', type=float, default=0.5)
    parser.add_argument('--sample_num', type=int, default=1)
    
    ## Flat cluster parameters
    
    parser.add_argument('--wildtype_prior', type=float)
    parser.add_argument('--wildtype_eps', type=float)
    parser.add_argument('--thin', default=1)
    
    ## Prior parameters
    parser.add_argument('--ref_prior', choices=['normal', 'variant', 'normal_variant'], default='normal_variant',
                        help='''Method for setting the copy number of the reference population. "normal" sets it
                                to the value of "normal_cn" in the input file. "variant" sets it to the sum of
                                "minor_cn" and "major_cn" in the input file. "normal_variant" considers both options
                                with equal prior weight. This option only has an effect if "total_copy_number" or
                                "no_zygosity is used for the var_prior option. Default normal_variant.''')
    
    parser.add_argument('--var_prior',
                        choices=['AB', 'BB', 'no_zygosity', 'parental_copy_number', 'total_copy_number'],
                        default='total_copy_number', help='''Method used to set the possible genotypes  of the
                                variant population. "AB" assumes all mutations have the AB genotype. "BB" assumes all
                                mutations have the BB genotype. "no_zygosity" assumes all mutation have the genotype
                                with the predicted total copy number and one mutant allele i.e. AAB for a copy number 3
                                mutation. "parental_copy_number" sets considers all possible genotypes compatible with
                                the predicted parental copy number. "total_copy_number" considers all possible genotypes
                                compatible with the predicted total copy number. If reliable parental copy number is
                                available the parental_copy_number method should be chosen. Default is
                                total_copy_number.''')
    
    ## perturb parameters
    parser.add_argument('--remove_clusters', default=0, type = int)
    parser.add_argument('--noise_sd', default=0, type = float)
    
    
    args = parser.parse_args()
    random = RandomState(args.seed)
    
    ## Generate simulated data
    data = data_simulation.simulate_data_tsv(args)
    org_tau = data[2]
    ## Perturb the input data
    if (args.remove_clusters > 0):
        data = remove_clusters(data, args.remove_clusters, random)
    
    if (args.noise_sd > 0):
        data = add_noise(data, args.noise_sd, random)
    
    ## Generate sample-file priors
    mutations = generate_priors(data[0], args)

    pyData = Pyc_inputs.Pyclone_inputs.inPlace(mutations, args.tumour_content, args.error_rate)
    
    
    ## Generate flat cluster file
    cluster_dict = OrderedDict()
    for sample in data[1].keys():
        M = len(data[1][sample])
        cluster_dict[sample] = {'mcmc': 1, 'phi': data[1][sample][0:(M - 1)], 'prior': data[2][0:(M - 1)]}
        
    phi_tau_df = pd.DataFrame(columns= ["Sample", "Cluster", "Prevalence"])
    for sample in data[1].keys():
        M = len(data[1][sample])
        
        for cluster_index in range(M):
            elem1 = data[1][sample][cluster_index]
            phi_tau_df = phi_tau_df.append({"Sample":sample, "Cluster": cluster_index, "Prevalence":elem1}, ignore_index= True)
            
    phi_array = np.array([np.append(data[1][sample][0:(M - 1)], [0]) for sample in data[1].keys()])
    phi_array_t = phi_array.transpose()
    phi_dist = distance.cdist(phi_array_t, phi_array_t, 'euclidean')
    
    
    
    #save_dir = '/Users/fdorri/Documents/UBC/projects/Simulations/muClone_simulation/results'
    save_dir = os.environ.get('SPECIFIED_RESULT_FOLDER')
    if not os.path.exists(save_dir):
        os.makedirs(save_dir)

    DELIM = ','

################################

    phi_tau_info = open(os.path.join(save_dir, 'samples_phi_tau_info.csv'), 'w')
    header = 'normal_prior' + DELIM + 'normal_eps' + DELIM + 'noise_sd' + DELIM + 'cluster_removed' +  DELIM + 'sample' + DELIM + 'cluster_name' + DELIM + 'prevalence'
    phi_tau_info.write(header + "\n")
        
    result_sample = open(os.path.join(save_dir, 'samples_fpr_tpr.csv'), 'w')
    header = 'normal_prior' + DELIM + 'normal_eps' + DELIM + 'noise_sd' + DELIM + 'cluster_removed' + DELIM + 'sample'  + DELIM + 'tpr' + DELIM + 'fpr' + DELIM + 'thrshld'
    result_sample.write(header + "\n")
         
    perform = open(os.path.join(save_dir, 'performance.csv'), 'w')    
    header = 'normal_prior' + DELIM + 'normal_eps' + DELIM + 'noise_sd' + DELIM + 'cluster_removed' + DELIM + 'misclassified_index' + DELIM + 'threshold' + DELIM + 'sensitivity' + DELIM + 'specificity' + DELIM + 'phi_threshod'
    perform.write(header + "\n")
    
    FN_file =  open(os.path.join(save_dir, 'false_negative_clustering.csv'), 'w')
    header = 'normal_prior' + DELIM + 'normal_eps' + DELIM + 'noise_sd' + DELIM + 'cluster_removed' + DELIM + 'sample' + DELIM + 'cluster_name' + DELIM + 'false_negatives'
    FN_file.write(header + "\n")
    
    
    cluster_file =  open(os.path.join(save_dir, 'cluster.csv'), 'w')
    header_list = ['normal_prior', DELIM, 'normal_eps', DELIM, 'noise_sd', DELIM, 'cluster_removed',DELIM ,'cluster_name', DELIM]
    num_dic = {'0':'zero', '1':'one', '2':'two', '3':'three', '4':'four', '5':'five', '6':'six', '7':'seven', '8':'eight', '9':'nine',  '10':'ten', '11':'eleven', '12':'twelve'}
    for i in range(args.cluster_num):
        name = 'cluster_' + num_dic[str(i)]
        header_list.append(name)
        header_list.append(DELIM)
        
    header_list.append('wildtype_cluster')
    header = ''
    for elem in header_list:
        header = header + elem
        
        
    cluster_file.write(header + '\n')  
               
    
    settings = str(args.wildtype_prior) + DELIM + str(args.wildtype_eps) + DELIM + str(args.noise_sd) + DELIM + str(args.remove_clusters)
##################################
    
    pyclone_mcmc = Pyc_mcmc.Pyclone_mcmc.fromFlatData(cluster_dict, args.precision)
    sample_ids = pyData.sample_ids
    mutation_classification = classification_engine(pyclone_mcmc, args.thin, args.wildtype_prior,
                                                    args.wildtype_eps, pyData, sample_ids, args.phi_threshold, False, 0, 0, args.seed)
#################################
    performance, performance_sample, fpr, tpr, th = analyse_sample(data, mutation_classification['sample_specific'], sample_ids, args.threshold)
    #FN_info = FN_analyse(data, mutation_classification['posterior']) 
    FN_info = FN_analyse_sample(data, mutation_classification['sample_specific'], sample_ids, args.threshold, args.remove_clusters, org_tau)
    cluster_data, cluster_name = cluster_assignment(data, mutation_classification, args.remove_clusters)
###############################
    for index, row in phi_tau_df.iterrows():
        res = str(row['Sample']) + DELIM + str(row['Cluster']) + DELIM + str(row['Prevalence'])
        phi_tau_info.write(settings + DELIM + res + '\n')
    phi_tau_info.close()
    
        
        

###############################


    for sample in data[1].keys():
        for t, f, th_elem in zip(tpr[sample],fpr[sample], th[sample]):
            res = str(sample) + DELIM + str(t) + DELIM + str(f) + DELIM + str(th_elem)
            result_sample.write(settings + DELIM + res + '\n')
    result_sample.close()
         
 
 
##############################
 
    for sample in FN_info.keys():
        for c, number in zip(FN_info[sample]['cluster'], FN_info[sample]['FN_num']):
            FN_file.write(settings + DELIM + str(sample) + DELIM + str(c) +  DELIM +  str(number) + '\n')
    FN_file.close()
     

    
    for c, cluster_vec in zip(cluster_name, cluster_data):
        res = str(c)
        for elem in cluster_vec:
            res = res + DELIM + str(elem)
        cluster_file.write(settings + DELIM + res + '\n')  
    cluster_file.close()
    
    misclass_index = 0
    if (args.remove_clusters == 0):        
        misclassified = (np.array(cluster_data).sum(axis=1) - np.array(cluster_data).diagonal())
        max_phi_dist = np.amax(phi_dist, axis=0)  
        #phi_dist[np.diag_indices_from(phi_dist)] = float('inf')
        min_phi_dist =  np.amin(phi_dist, axis=0) 
        #phi_dist[np.diag_indices_from(phi_dist)] = 0
        max_cost  = sum(np.multiply(misclassified, max_phi_dist))
        min_cost = sum(np.multiply(misclassified, min_phi_dist))
        #min_cost should be zero
        
        
        cost_matrix = np.multiply(cluster_data, phi_dist) 
        
        sum_cost_matrix = (cost_matrix.sum(axis=1) - cost_matrix.diagonal()).sum()
     
        try:
            misclass_index = (sum_cost_matrix - min_cost) / (max_cost - min_cost)
        except:
            misclass_index = 0
            
        
        phi_dist_df = pd.DataFrame(phi_dist)
        phi_dist_df.to_csv(os.path.join(save_dir, 'phi_dist.csv'), header=None)
        
        dist_df = pd.DataFrame(cost_matrix)
        dist_df.to_csv(os.path.join(save_dir, 'cost.csv'), header=None)  
        
        
    results = str(misclass_index) + DELIM + str(args.threshold) + DELIM + str(performance['sensitivity']) + DELIM + str(performance['specificity']) + DELIM + str(args.phi_threshold)                                                
    perform.write(settings + DELIM + results + '\n')
    perform.close()

##############################
    
    
    
    
    write_options(args)
    



