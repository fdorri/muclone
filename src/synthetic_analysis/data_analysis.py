'''
Created on Nov 25, 2015
    
@author: fdorri
'''

def analyse_sample(data, sample_specific, sample_ids, threshold):

    simulated_data = data[3]
    from sklearn.metrics import roc_curve
    
    ground_truth_data = {}
    result = {}
    
    performance = {'sensitivity': 0, 'specificity': 0} 
    performance_sample = {}
    
    
    fpr = {}
    tpr = {}
    th = {}
    for sample in sample_ids:
        ground_truth_data[sample] = []
        result[sample] = []
        for elem in sample_specific.index:
            gt = simulated_data[elem][sample]
            r = sample_specific.loc[elem].loc[sample]          
            ground_truth_data[sample].append(gt)
            result[sample].append(r)


            
    for sample in sample_ids:
        TP = 0
        FP = 0
        TN = 0
        FN = 0
        
        for gt, r in zip(ground_truth_data[sample], result[sample]):
            if int(float(gt)) == 1:
                if float(r) > threshold:
                    TP += 1
                else:
                    FN += 1
            if int(float(gt)) == 0:
                if float(r) > threshold:
                    FP += 1
                else:
                    TN += 1
        sensitivity = float(TP) / (TP + FN)
        specificity = float(TN) / (TN  + FP)
        performance_sample[sample] = {'sensitivity': sensitivity, 'specificity': specificity} 
        performance = {'sensitivity': performance['sensitivity']+ sensitivity, 'specificity': performance['specificity'] + specificity} 
              
    performance['sensitivity'] = performance['sensitivity'] / len(sample_ids)
    performance['specificity'] = performance['specificity'] / len(sample_ids)
    
    for sample in sample_ids:
        fpr[sample] , tpr[sample], th[sample] = roc_curve(ground_truth_data[sample], result[sample])
    
    
    
    return performance, performance_sample, fpr, tpr, th


def analyse(simulated_data, muclone_posterior):
    
    ## Assumes type is set based on all samples (for now)
    simed = pd.DataFrame.from_dict(simulated_data, orient='index', dtype=str)
    simed.reset_index(level=0, inplace=True)
    simed.columns = ['mutation_id', 'type']
    muclone_posterior.index.name = 'mutation_id'
    muclone_posterior.reset_index(inplace=True)
    df = pd.merge(simed, muclone_posterior, on='mutation_id')
    somatic = df[(df.type == "True")]
    wildtype = df[(df.type == "False")]
    
    sensitivity = sum(somatic.decision) / len(somatic)
    specificity = 1 - sum(wildtype.decision) / len(wildtype)
    #print "sensi= " + str(sensitivity) + "speci= " + str(specificity)
    return {'sensitivity': sensitivity, 'specificity': specificity}

def cluster_assignment(data, muclone_data, remove_cluster):
    
    samples = data[0].keys()
    cluster_data = {}
    for elem, cluster, decision in zip(muclone_data['cluster_label_info']['mutations'], muclone_data['cluster_label_info']['mcmc_iter_1'],  muclone_data['posterior']['decision']):        #decision = muclone_data['posterior'].loc(elem)['decision']
        cluster_data[elem] = {}
        if cluster < 1:
            cluster_data[elem]['muclone_cluster'] = cluster
        else:
            cluster_data[elem]['muclone_cluster'] = cluster + remove_cluster
            
        cluster_data[elem]['decision'] = decision
    
    for mut, cluster_org in zip(data[0][samples[0]]['mutation_id'],data[0][samples[0]]['cluster']):
        label = data[3][mut]
        if mut in cluster_data.keys():
            cluster_data[mut]['label'] = label
            cluster_data[mut]['cluster'] = cluster_org            

            
    
    cluster = data[0][samples[0]]['cluster']
    max_cluster = max(cluster)
    

    cluster_matrix = [[0 for x in range(max_cluster + 2)]for x in range(max_cluster + 2)]
    
    
    for elem in cluster_data.keys():
        a = int(float(cluster_data[elem]['cluster']))
        b = int(float(cluster_data[elem]['muclone_cluster']))
        if str(a) =='-1':
            a = max_cluster + 1
        if str(b) == '-1':
            b = max_cluster + 1
            
        cluster_matrix[a][b] += 1
        
        
    cluster_name = [str(i) for i in range(max_cluster + 1)]
    cluster_name.append('-1')
        
    return cluster_matrix, cluster_name
            
   
def FN_analyse_sample(data, sample_specific, sample_ids, threshold, remove_cluster, org_tau):  
    FN_dict = {}
    FN_data = {}
    cluster_name = {}
    simulated_data = data[3]
    
     
        
    
    for sample in sample_ids:
        cluster = data[0][sample]['cluster']
        max_cluster = max(cluster)
        cluster_name[sample] = [str(i) for i in range(max_cluster + 1)] 
        

    
    for sample in sample_ids:
        FN_dict[sample] = {}
        FN_data[sample] = {}
        for cluster in cluster_name[sample]:
            FN_dict[sample][cluster] = 0
        
        for elem, cluster in zip(data[0][sample]['mutation_id'], data[0][sample]['cluster']) :
            gt = simulated_data[elem][sample]
            r = sample_specific.loc[elem].loc[sample]
            #cluster = sample_specific.loc[elem].loc['cluster']
            if gt == 1 and r < threshold:
                FN_dict[sample][str(cluster)] += 1
  
                    
    for sample in sample_ids:
        FN_data[sample]['cluster'] = []
        FN_data[sample]['FN_num'] = []
        for cluster in cluster_name[sample]:
            cluster_size = org_tau[int(float(cluster))]
            val = float(FN_dict[sample][str(cluster)])/ float(cluster_size)
            FN_data[sample]['FN_num'].append(val)
            FN_data[sample]['cluster'].append(cluster)

                
              
    return FN_data

def FN_analyse(data, muclone_posterior, remove_cluster, org_tau):

    
    samples = data[0].keys()
    cluster = data[0][samples[0]]['cluster']
    max_cluster = max(cluster)
    
    data_dict = {}
    data_dict['type'] = []
    data_dict['mutation_id'] = data[0][samples[0]]['mutation_id']
    for elem in data_dict['mutation_id']:
        data_dict['type'].append(data[3][elem])
    data_dict['cluster'] = cluster
    df = pd.DataFrame.from_dict(data_dict, orient='columns', dtype=str)

    df.columns = ['cluster', 'mutation_id', 'type']
    data_df = pd.merge(df, muclone_posterior)
    
    
    cluster_name = [str(i) for i in range(max_cluster + 1)]  
    #cluster_name.append('-1') 
    
    FN_data = {}
    FN_data['cluster'] = cluster_name
    #FN_data['phi'] = data[1] 


    data_df['type'] = data_df['type'].astype(str)
    
    somatic = data_df[(data_df.type == "True")]
    wildtype = data_df[(data_df.type == "False")]
    
    FN = somatic[(somatic.decision == 0)]
    FN_cluster =  FN['cluster'].astype(str)
    
    FN_list =[]
    for c, t in zip(cluster_name, org_tau):
        FN_num = len(FN[(FN_cluster == str(c))])
        FN_list.append(float(FN_num)/float(t))
    
    FN_data['FN_num'] = FN_list
    return FN_data
