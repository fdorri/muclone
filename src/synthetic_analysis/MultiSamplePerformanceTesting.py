'''
Created on Feb 18, 2015
    
@author: jewellsean
'''
from __future__ import division
import argparse
import math
import os

import pandas as pd
import numpy as np
from collections import OrderedDict
from pyclone.config import get_mutation

from mutation_classifier.classification_engine import classification_engine
from pyclone_data import Pyclone_inputs
from pyclone_data import Pyclone_mcmc
from utils.IO_Utils import write_options
from simulation import data_simulation


def generate_priors(data, args):
    mutations = OrderedDict()
    
    for sample in data.keys():
        mutations[sample] = []
        nMutations = len(data[sample]['normal_cn'])
        
        for i in range(nMutations):
            mutation_id = data[sample]['mutation_id'][i]
            
            ref_counts = int(data[sample]['ref_counts'][i])
            
            var_counts = int(data[sample]['var_counts'][i])
            
            normal_cn = int(data[sample]['normal_cn'][i])
            
            minor_cn = int(data[sample]['minor_cn'][i])
            
            major_cn = int(data[sample]['major_cn'][i])
            
            mutation = get_mutation(mutation_id,
                                    ref_counts,
                                    var_counts,
                                    normal_cn,
                                    minor_cn,
                                    major_cn,
                                    args.ref_prior,
                                    args.var_prior)
                                    
            mutations[sample].append(mutation.to_dict())
    return mutations

def analyse(simulated_data, muclone_posterior):
    ## Assumes type is set based on all samples (for now)
    simed = pd.DataFrame.from_dict(simulated_data, orient = 'index', dtype = str)
    simed.reset_index(level=0, inplace=True)
    simed.columns = ['mutation_id', 'type']
    muclone_posterior.index.name = 'mutation_id'
    muclone_posterior.reset_index(inplace=True)
    df = pd.merge(simed, muclone_posterior, on = 'mutation_id')
    somatic = df[(df.type == "True")]
    wildtype = df[(df.type == "False")]
    
    sensitivity = sum(somatic.decision) / len(somatic)
    specificity = 1 - sum(wildtype.decision) / len(wildtype)
    
    return {'sensitivity' : sensitivity, 'specificity' : specificity}

def log_range(aMin, bMax, incr):
    a = math.log10(aMin)
    b = math.log10(bMax)
    return np.arange(a, b, incr)


if __name__ == '__main__':
    
    ## Data simulation parameters
    
    parser = argparse.ArgumentParser(prog='MuCloneMultiSampleTesting')
    parser.add_argument('--hg_dir', help = '''the reference genome directory''')
    parser.add_argument('--pos_num', type=int, help='''the number of positions''')
    parser.add_argument('--cluster_num', type=int, help='''the number of clusters''')
    parser.add_argument('--tumour_content', type=float, help='''tumour_content''')
    parser.add_argument('--depth', type=int, help='''average depth of the data''')
    parser.add_argument('--max_copy', type=int, help='''max copy number''')
    parser.add_argument('--wildtype_phi', type=float, default=0, help='''mu for wildtype cluster''')
    parser.add_argument('--max_wildtype_number', type=int, help='''number of wildtype position in simulated data''')
    parser.add_argument('--precision', type=int, default=10000, help='''precision parameter for beta-binomial''')
    parser.add_argument('--error_rate', type=float, help='''error rate for accounting for sequencing error''')
    parser.add_argument('--seed', type=int, default = 1)
    parser.add_argument('--sample_num', type=int, default = 1)
    
    ## Flat cluster parameters
    
    parser.add_argument('--normal_prior_min', type=float)
    parser.add_argument('--normal_prior_max', type=float)
    parser.add_argument('--normal_prior_incr', type=float)
    parser.add_argument('--normal_epsilon_min', type=float)
    parser.add_argument('--normal_epsilon_max', type=float)
    parser.add_argument('--normal_epsilon_incr', type=float)
    parser.add_argument('--thin', default=1)
    
    ## Prior parameters
    parser.add_argument('--ref_prior', choices=['normal', 'variant', 'normal_variant'], default='normal_variant',
                                help='''Method for setting the copy number of the reference population. "normal" sets it
                                to the value of "normal_cn" in the input file. "variant" sets it to the sum of
                                "minor_cn" and "major_cn" in the input file. "normal_variant" considers both options
                                with equal prior weight. This option only has an effect if "total_copy_number" or
                                "no_zygosity is used for the var_prior option. Default normal_variant.''')
    
    parser.add_argument('--var_prior',
                                choices=['AB', 'BB', 'no_zygosity', 'parental_copy_number', 'total_copy_number'],
                                default='total_copy_number', help='''Method used to set the possible genotypes  of the
                                variant population. "AB" assumes all mutations have the AB genotype. "BB" assumes all
                                mutations have the BB genotype. "no_zygosity" assumes all mutation have the genotype
                                with the predicted total copy number and one mutant allele i.e. AAB for a copy number 3
                                mutation. "parental_copy_number" sets considers all possible genotypes compatible with
                                the predicted parental copy number. "total_copy_number" considers all possible genotypes
                                compatible with the predicted total copy number. If reliable parental copy number is
                                available the parental_copy_number method should be chosen. Default is
                                total_copy_number.''')
    
    
    args = parser.parse_args()
    
    ## Generate simulated data
    data = data_simulation.simulate_data_tsv(args)
    
    ## Run Classification engine
    
    ## Generate sample-file priors
    mutations = generate_priors(data[0], args)
    
    pyData = Pyclone_inputs.Pyclone_inputs.inPlace(mutations, args.tumour_content, args.error_rate)
    
    
    ## Generate flat cluster file
    cluster = OrderedDict()
    for sample in data[1].keys():
        M = len(data[1])
        cluster[sample] = {'mcmc' : 1, 'phi' : data[1][sample][0:(M-1)], 'prior' : data[2][0:(M-1)]}
    
    save_dir = os.environ.get('SPECIFIED_RESULT_FOLDER')
    if not os.path.exists(save_dir):
        os.makedirs(save_dir)
    
    DELIM = ','
    
    perform = open(os.path.join(save_dir, 'performance.csv'), 'w')
    header = 'normal_prior' + DELIM + 'normal_eps' + DELIM + 'sensitivity' + DELIM + 'specificity'
    perform.write(header + "\n")
    norm_prior_array = np.append(np.arange(args.normal_prior_min, args.normal_prior_max, args.normal_prior_incr), 0.999999)

    for norm_prior in norm_prior_array:
        for norm_eps in log_range(args.normal_epsilon_min, args.normal_epsilon_max, args.normal_epsilon_incr):
            pyclone_mcmc = Pyclone_mcmc.Pyclone_mcmc.fromFlatData(cluster, args.precision)
            sample_ids = pyData.sample_ids
            mutation_classification = classification_engine(pyclone_mcmc, args.thin, norm_prior, math.pow(10, norm_eps), pyData, sample_ids)
            performance = analyse(data[3], mutation_classification['posterior'])
            settings = str(norm_prior) + DELIM + str(math.pow(10, norm_eps))
            results =  str(performance['sensitivity']) + DELIM + str(performance['specificity'])
            perform.write(settings + DELIM +  results + '\n')
    perform.close()
    write_options(args)



