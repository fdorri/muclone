'''
Created on Jan 27, 2015

Test the beta binomial implementation


@author: jewellsean
'''
from pyclone import pyclone_beta_binomial
from math import exp

def check_bb(d, param_a, param_b):
    prob = 0
    for b in range(1,d + 1):
        prob += exp(pyclone_beta_binomial.log_beta_binomial_pdf(b, d, param_a, param_b))
    print "d: " + str(d) + " alpha: " + str(param_a) + " beta: " + str(param_b) + " prob: " + str(prob)


if __name__ == '__main__':
    for d in range(1, 100):
        for param_a in range(1, 10):
            for param_b in range(1,10):
                check_bb(d, param_a, param_b)