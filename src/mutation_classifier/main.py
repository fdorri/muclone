'''
Created on Feb 2, 2015
Updated on July, 2017

- classification_engine is the main engine to evaluate the MuClone decision process

analysis
    - useful when the clustering (population structure) is from another source. potentially pyClone, BitPhylogeny or otherwise
    - also useful when testing against simulated data. in this case the true population structure is *known* 
@author: fdorri, jewellsean
'''
import argparse
from run import analysis


def _setup_analysis_parser(analysis_parser):
    analysis_parser.add_argument('--precision', default=1000)
    analysis_parser.add_argument('--config_file')
    analysis_parser.add_argument('--wildtype_prior', default=0.5)
    analysis_parser.add_argument('--sample_name', default='test')
    analysis_parser.add_argument('--phi_threshold', default=0.02, type=float)
    analysis_parser.add_argument('--perturb_input', action='store_true', default= False)
    analysis_parser.add_argument('--remove_cluster', default=0, type=float)
    analysis_parser.add_argument('--add_noise', default=0, type=float)
    analysis_parser.add_argument('--seed', default=100, type=int)
    analysis_parser.set_defaults(func=analysis)




    ######

parser = argparse.ArgumentParser(prog='MuClone')

parser.add_argument('--version', action='version', version='Muclone-0.1')

subparsers = parser.add_subparsers()

#########################
analysis_parser = subparsers.add_parser('analysis', help='''run muClone when clusters information is from another source''')
_setup_analysis_parser(analysis_parser)



########################
args = parser.parse_args()
args.func(args)





