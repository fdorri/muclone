'''
Created on Feb 2, 2015
Updated on July, 2017

- classification_engine is the main engine to evaluate the MuClone decision process

analysis
    - useful when the clustering (population structure) is from another source. potentially pyClone, BitPhylogeny or otherwise
    - also useful when testing against simulated data. in this case the true population structure is *known* 
@author: fdorri, jewellsean
'''

import yaml
from mutation_classifier.classification_engine import classification_engine
from pyc_data import Pyc_inputs
from pyc_data import Pyc_mcmc
from utils.IO_Utils import write_results


def analysis(args):   
    thin = 1  
    config_file = open(args.config_file)
    config = yaml.load(config_file)
    
    data = Pyc_inputs.Pyclone_inputs.fromConfig(config)
    pyclone_mcmc = Pyc_mcmc.Pyclone_mcmc.fromFlat(data.flat_cluster_files, args.precision, args.sample_name)
    sample_ids = data.sample_ids
  
    muclone_results = classification_engine(pyclone_mcmc, thin, args.wildtype_prior, 0, data, sample_ids, float(args.phi_threshold), args.perturb_input, args.remove_cluster, args.add_noise, args.seed)
    write_results(config, args, muclone_results)


