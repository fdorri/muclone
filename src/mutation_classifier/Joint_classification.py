'''
Created on Sep 16, 2014

@author: fdorri, jewellsean

'''
from __future__ import division
from collections import OrderedDict
from utils import PyClone_Beta_Binomial_Utils
from utils import DictReorder
from utils import Math
from math import exp
def joint_classification(pyclone_data, cluster2Phi, epsilon_mu, phi_threshold):

    
    common_mutations = pyclone_data.common_mutations
    cluster_ids = cluster2Phi.cluster_ids
    sample_ids = pyclone_data.sample_ids 
    sample_data = pyclone_data.sample_data
    
    cluster_switch = DictReorder.switch_dict_axes(cluster2Phi.cluster_prevalence,
                                                  'sample_id', 'cluster_id',
                                                  sample_ids, cluster_ids)
    mutation_switch = DictReorder.switch_dict_axes(sample_data,
                                                   'sample_id',
                                                   'mutation_id',
                                                   sample_ids,
                                                   common_mutations)
    sample_phi = OrderedDict()
    cluster_posterior = OrderedDict()
    cluster_prob = OrderedDict()
    cluster_post = OrderedDict()
    sample_specific_result = OrderedDict()
    collapsed_cluster_posterior = OrderedDict()
    collapsed_info = OrderedDict() 
    for mutation_id in common_mutations:
        cluster_posterior[mutation_id] = OrderedDict()
        for cluster_id in cluster_ids:
            if (cluster_id == -1):
                normal = True
            else:
                normal = False
            cluster_posterior[mutation_id][cluster_id] = PyClone_Beta_Binomial_Utils.posterior_cluster_likelihood(cluster_switch[cluster_id],
                                                                    mutation_switch[mutation_id],
                                                                    cluster2Phi.precision,
                                                                    epsilon_mu,
                                                                    normal)
    
    for mutation_id in common_mutations:
        cluster_post = OrderedDict()
        normal = 0 
        log_probMutant = []
        log_probMutant_prior = []
        log_probMutant_likelihood = []
        sample_phi[mutation_id] = OrderedDict()
        collapsed_cluster_posterior[mutation_id] = OrderedDict()
        collapsed_info[mutation_id] = OrderedDict()
        sample_specific_result[mutation_id] = OrderedDict()
        max_tumour_cluster = -2
        log_max_tumour_liklihood = -1000000
        for cluster_id in cluster_ids:
            if cluster_id == -1:
                normal_likelihood = cluster_posterior[mutation_id][cluster_id][0]
                normal_prior = cluster_posterior[mutation_id][cluster_id][1]
                normal = normal_likelihood + normal_prior
                cluster_prob[cluster_id] = normal
            else:
                clust_post = cluster_posterior[mutation_id][cluster_id][1] + cluster_posterior[mutation_id][cluster_id][0]
                cluster_prob[cluster_id] = clust_post
                if clust_post >=  log_max_tumour_liklihood:
                    log_max_tumour_liklihood = clust_post
                    max_tumour_cluster = cluster_id                 
                    
                log_probMutant_likelihood.append(cluster_posterior[mutation_id][cluster_id][0])
                log_probMutant_prior.append(cluster_posterior[mutation_id][cluster_id][1])
                log_probMutant.append(cluster_posterior[mutation_id][cluster_id][1] + cluster_posterior[mutation_id][cluster_id][0])
        mutant_prior = Math.log_sum_exp(log_probMutant_prior)
        mutant_likelihood = Math.log_sum_exp(log_probMutant_likelihood)
        mutant = Math.log_sum_exp(log_probMutant) 
        temp = log_probMutant 
        temp.append(normal)
        total = Math.log_sum_exp(temp)
        
        for cluster_id in cluster_ids:
            cluster_post[cluster_id] = cluster_prob[cluster_id] - total
            
             

        collapsed_cluster_posterior[mutation_id]['normal_prior'] = normal_prior
        collapsed_cluster_posterior[mutation_id]['normal_likelihood'] = normal_likelihood
        collapsed_cluster_posterior[mutation_id]['normal'] = normal
        collapsed_cluster_posterior[mutation_id]['mutant_prior'] = mutant_prior
        collapsed_cluster_posterior[mutation_id]['mutant_likelihood'] = mutant_likelihood
        collapsed_cluster_posterior[mutation_id]['mutant'] = mutant
        liklihoods = log_probMutant
        collapsed_info[mutation_id]['log_likelihoods'] = liklihoods
        if normal >  mutant: 
            collapsed_cluster_posterior[mutation_id]['decision'] = 0
            collapsed_info[mutation_id]['cluster'] = -1            
        else:
            collapsed_cluster_posterior[mutation_id]['decision'] = 1
            collapsed_info[mutation_id]['cluster'] = max_tumour_cluster
        #*****
        cluster = collapsed_info[mutation_id]['cluster']
        sample_specific_result[mutation_id]['cluster'] = cluster
        sample_specific_result[mutation_id]['cluster_prob'] = cluster_post
        for s in pyclone_data.sample_ids:
            prob_vec=[]
            if normal >  mutant:
                for cluster_id in cluster_ids:
                    if cluster_switch[cluster_id][s][0] < phi_threshold:
                        prob_vec.append(sample_specific_result[mutation_id]['cluster_prob'][cluster_id])                   
                sample_phi[mutation_id][s] = 1- exp(Math.log_sum_exp(prob_vec))
            else:
                for cluster_id in cluster_ids:            
                    if cluster_switch[cluster_id][s][0] > phi_threshold:
                        prob_vec.append(sample_specific_result[mutation_id]['cluster_prob'][cluster_id])
                sample_phi[mutation_id][s] = exp(Math.log_sum_exp(prob_vec)) 
            sample_specific_result[mutation_id][s] = sample_phi[mutation_id][s]


            
            
    return collapsed_cluster_posterior, collapsed_info, sample_specific_result
