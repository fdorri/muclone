'''
Created on Jan 30, 2015
Updated on July, 2017

- classification_engine is the main engine to evaluate the MuClone decision process

analysis
    - useful when the clustering (population structure) is from another source. potentially pyClone, BitPhylogeny or otherwise
    - also useful when testing against simulated data. in this case the true population structure is *known* 
@author: fdorri, jewellsean
'''
from __future__ import division
import pandas as pd
import numpy as np
from numpy.random import RandomState
import random
import os
from mcmc_processor import Cluster_cellular_prevalence
from mutation_classifier.Joint_classification import joint_classification


def perturb_cellular_prevalance(cluster2Phi, remove_cluster, add_noise, seed):

    random = RandomState(seed)
    #random.seed(seed)
    #sample_num = len(cluster2Phi.cluster_prevalence.keys())
    cluster_num = max([len(x) for x in cluster2Phi.cluster_prevalence.values() ])
    rm_cluster = int(remove_cluster*cluster_num)
    if rm_cluster >= cluster_num-1  :
        rm_cluster = cluster_num-2
        #raise Exception('the number of clones to be removed are more than what is allowed')
    #rm_clones = random.sample(range(cluster_num-1), rm_cluster)
    rm_clones = [i + 1 for i in range(rm_cluster)]
    tau = {}
    for elem in rm_clones:
        cluster2Phi.cluster_ids.remove(elem)
        for sample in cluster2Phi.cluster_prevalence.keys():
            try:
                tt = cluster2Phi.cluster_prevalence[sample][elem][1]
                del cluster2Phi.cluster_prevalence[sample][elem]
                if sample in tau.keys():
                    tau[sample].append(tt)
                else:
                    tau[sample]= [tt]

                sample_list.append(sample)
            except:
                continue

    for sample in tau.keys():
        tau_sum = sum(tau[sample])
        for elem in cluster2Phi.cluster_prevalence[sample].keys():
            cluster2Phi.cluster_prevalence[sample][elem] = (cluster2Phi.cluster_prevalence[sample][elem][0], cluster2Phi.cluster_prevalence[sample][elem][1]/ (1-tau_sum))

    if add_noise>0:
        for sample in cluster2Phi.cluster_prevalence.keys():
            for elem in cluster2Phi.cluster_prevalence[sample].keys():
                if elem >0:
                    val = cluster2Phi.cluster_prevalence[sample][elem][0]
                    #if elem >0:
                    coin = random.choice([True, False])
                    if coin :
                        val = cluster2Phi.cluster_prevalence[sample][elem][0] + random.normal(loc=0, scale=add_noise)
                    elif coin :
                        val = cluster2Phi.cluster_prevalence[sample][elem][0] + random.normal(loc=0, scale=add_noise)

                    if val >1:
                        val = 1
                    elif val < 0 :
                        val = 0
                    cluster2Phi.cluster_prevalence[sample][elem] = (val, cluster2Phi.cluster_prevalence[sample][elem][1])

    return cluster2Phi














def classification_engine(pyclone_mcmc, thin, normal_prior, normal_epsilon, data, sample_ids, phi_threshold, perturb_input, remove_cluster, add_noise, seed):

    first = 1
    mcmcSteps = 0

    for i in range(pyclone_mcmc.mcmc_iter + 1):
        if i > 0:
            mcmc_iter = i
            if (mcmc_iter % int(thin) == 0):
                mcmcSteps = mcmcSteps + 1
                one_mcmc_cluster2Phi_0 = Cluster_cellular_prevalence.Cluster_cellular_prevalence(sample_ids,
                                                                        pyclone_mcmc.mcmc_cluster2Phi,
                                                                        mcmc_iter,
                                                                        pyclone_mcmc.precision[mcmc_iter], normal_prior)
                if perturb_input:
                    one_mcmc_cluster2Phi = perturb_cellular_prevalance(one_mcmc_cluster2Phi_0, remove_cluster, add_noise, seed)
                else:
                    one_mcmc_cluster2Phi = one_mcmc_cluster2Phi_0

                cluster_posterior, cluster_info, sample_specific_result = joint_classification(data, one_mcmc_cluster2Phi, normal_epsilon, phi_threshold)
                cluster_index = []
                cluster_liklihoods = []
                for mutation in cluster_info.keys():
                    cluster_index.append(cluster_info[mutation]['cluster'])
                    cluster_liklihoods.append(cluster_info[mutation]['log_likelihoods'])
                    
                    

                
                
                mutation_posterior_temp = pd.DataFrame(cluster_posterior)
                sample_specific_result_temp = pd.DataFrame(sample_specific_result)
                col_name = 'mcmc_iter_' + str(mcmc_iter)

                if first == 1:
                    mutation_posterior = mutation_posterior_temp.T
                    sample_specific_result = sample_specific_result_temp.T
                    cluster_label_info = pd.DataFrame({'mutations':cluster_info.keys(), col_name: cluster_index,'liklihoods':cluster_liklihoods})
                    cluster_label_info = cluster_label_info[['mutations', col_name,'liklihoods']]
                else: 
                    mutation_posterior = (mutation_posterior_temp.T + mutation_posterior)
                    sample_specific_result = (sample_specific_result_temp.T + sample_specific_result)
                    first_cluster_label_info = cluster_label_info[['mutations', col_name]]
                    temp = pd.DataFrame({'mutations':cluster_info.keys(), col_name: cluster_index})
                    cluster_label_info = pd.merge(first_cluster_label_info, temp, on=['mutations'], how='left')

                first = 0 

    mutation_posterior = mutation_posterior / mcmcSteps
    
    return {'posterior' : mutation_posterior, 'cluster_label_info' : cluster_label_info, 'sample_specific':sample_specific_result }
