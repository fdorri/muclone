'''
Created on May 16, 2015

@author: fdorri
'''
from random_loci import make_random_pos
import random_genotype
from numpy.random import RandomState
from numpy import random
import argparse
import yaml
from random import randint
from utils import DictReorder


def main(args):
    data = simulate_data_tsv(args)
    write_info_file(args, data[1], data[2], args.precision)   
    write_cluster2Phi(args, data[1], data[2])

    write_tsv(data[0], args.out_dir, args.pos_num, args.sample_name)
    write_labeled_tsv(data[0], args.out_dir, args.pos_num, args.sample_name)
    write_config_file(args)
    write_inputs(args)
    
    
def simulate_data_tsv(args):
    rand = RandomState(args.seed)
    positions = make_random_pos(rand, int(args.pos_num), 1, 10, args.hg_dir)
    wildtype_num = rand.randint(1, int(args.max_wildtype_number))
    wildtype_tau = float(wildtype_num) / args.pos_num
    tau, count_check = random_genotype.random_cellular_info(rand, args.pos_num, args.cluster_num, wildtype_tau) 
    mutations = {}
    phi = {}
    labels = {}
    #phi_threshold = float(2) * args.error_rate
    phi_threshold = args.phi_threshold
    tree_phi, tree_label = random_genotype.get_phi_tree(rand, args.sample_num, args.cluster_num, args.wildtype_phi, phi_threshold, args.error_rate)
    for i in range(args.sample_num):

        sample = "S" + str(i)
        #phi[sample], labels[sample] = random_genotype.get_phi_insreted_wildtype(rand, args.cluster_num, args.wildtype_phi, args.error_rate)
        phi[sample] = tree_phi[i]
        labels[sample] = tree_label[i]

        mutations[sample]= {}
        for key in ['mutation_id', 'ref_counts', 'var_counts', 'normal_cn', 'minor_cn', 'major_cn', 'total_cn', 'var_freq', 'type', 'cluster', 'label']:
            mutations[sample][key] = []
            
        depth = []
        cluster_id = range(args.cluster_num + 1)
        cluster_id[-1] = -1
        c_tot, c_major, c_minor = random_genotype.random_copy_number(rand, args.pos_num, args.max_copy)
        b = []
        precision_params = args.precision
    
        pos_index = -1
        for cluster_count, number in zip(cluster_id , count_check):        
            for i in range(int(number)):
                pos_index = pos_index + 1
                c_t = c_tot[pos_index]
                c_mj = c_major[pos_index]
                c_mn = c_minor[pos_index]
                exp_depth = float(args.depth) * c_t / 2
                d = random_genotype.get_depth(rand, 1, exp_depth)[0]
                depth.append(d)
                if cluster_count == -1:
                    m_id = 'Unknown'
                    label = labels[sample][cluster_count]
                    cellular_frequency = phi[sample][cluster_count]
                    cn_n, cn_r, cn_v, mu_n, mu_r, mu_v = random_genotype.get_genotype(rand, c_t, c_mj, c_mn, label, args.error_rate)
                else:
                    cellular_frequency = phi[sample][cluster_count]
                    m_id = 'Unknown'
                    label = labels[sample][cluster_count]
                    cn_n, cn_r, cn_v, mu_n, mu_r, mu_v = random_genotype.get_genotype(rand, c_t, c_mj, c_mn, label, args.error_rate)
                mu = calculate_mu(cellular_frequency, args.tumour_content, cn_n , mu_n, cn_r, mu_r, cn_v, mu_v)
                param_a = mu * precision_params
                param_b = (1 - mu) * precision_params
                p = rand.beta(param_a, param_b)
                b_element = rand.binomial(depth[pos_index], p)
                b.append(b_element)
                freq = float(b_element) / float(depth[pos_index])
                mutation_id = m_id + ":" + positions[pos_index]

            
                mutations [sample]['mutation_id'].append(mutation_id)
                mutations [sample]['ref_counts'].append(depth[pos_index] - b_element )
                mutations [sample]['var_counts'].append(b_element)
                mutations [sample]['normal_cn'].append(2)
                mutations [sample]['minor_cn'].append(c_minor[pos_index])
                mutations [sample]['major_cn'].append(c_major[pos_index])
                mutations [sample]['total_cn'].append(c_tot[pos_index])
                mutations [sample]['var_freq'].append(freq)
                mutations [sample]['type'].append(m_id)
                mutations [sample]['cluster'].append(cluster_count) 
                mutations [sample]['label'].append(label)

    label = labelled_mutations_vec(mutations, args.pos_num)
    return mutations, phi, tau, label


def calculate_mu(cellular_frequency, tumour_content, cn_n , mu_n, cn_r, mu_r, cn_v, mu_v):
    f = cellular_frequency
    t = tumour_content
        
    p_n = (1 - t) * cn_n
    p_r = t * (1 - f) * cn_r
    p_v = t * f * cn_v
        
    norm_const = p_n + p_r + p_v
        
    p_n = p_n / norm_const
    p_r = p_r / norm_const
    p_v = p_v / norm_const
    
    mu = p_n * mu_n + p_r * mu_r + p_v * mu_v  
    return mu

def write_tsv(data, out_dir, num, name):
    import csv
    import os
    tsv_fields = ['mutation_id', 'ref_counts', 'var_counts', 'normal_cn', 'minor_cn', 'major_cn', 'total_cn', 'var_freq', 'type', 'cluster', 'label']

    outdir =  os.path.join(out_dir, 'tsv', name)
    if not os.path.exists(outdir):
        os.makedirs(outdir)
        
        
    for sample in data.keys():
        filename = sample + '.tsv'
        outfile = os.path.join(outdir,  filename)

        output = csv.DictWriter(open(outfile, 'w'), fieldnames=tsv_fields ,  delimiter='\t')
        output.writeheader()
        for item in range(num):
            output.writerow({'mutation_id': data[sample]['mutation_id'][item], 'ref_counts': data[sample]['ref_counts'][item], 'var_counts':data[sample]['var_counts'][item], 
                             'normal_cn':data[sample]['normal_cn'][item], 'minor_cn':data[sample]['minor_cn'][item], 'major_cn':data[sample]['major_cn'][item], 
                             'total_cn':data[sample]['total_cn'][item], 'var_freq':data[sample]['var_freq'][item],'type':data[sample]['type'][item], 'cluster':data[sample]['cluster'][item],'label':data[sample]['label'][item] })
def labelled_mutations(data, num):
    sample_ids = data.keys()
    common_mutations = set.intersection(*[set(data[sample]['mutation_id'][item] for item in range(num)) for sample in data.keys()])
    label = {}

    for mutation in common_mutations:
        label[mutation] = False
        for sample in sample_ids:
            for item in range(num):
                if data[sample]['mutation_id'][item] == mutation:
                    label[mutation] = data[sample]['label'][item] or label[mutation]
                    break
    return label

def labelled_mutations_vec(data, num):
    sample_ids = data.keys()
    common_mutations = set.intersection(*[set(data[sample]['mutation_id'][item] for item in range(num)) for sample in data.keys()])
    label = {}

    for mutation in common_mutations:
        label[mutation] = {}
        for sample in sample_ids:
            for item in range(num):
                if data[sample]['mutation_id'][item] == mutation:
                    label[mutation][sample] = data[sample]['label'][item]
                    break
    return label


def write_labeled_tsv(data, out_dir, num, name):
    import csv
    import os
    tsv_fields = ['mutation_id', 'label']

    outdir =  os.path.join(out_dir, 'tmp', name)
    if not os.path.exists(outdir):
        os.makedirs(outdir)
        
    filename = name + '_labeled.tsv'
    outfile = os.path.join(outdir,  filename)
    
    output = csv.DictWriter(open(outfile, 'w'), fieldnames=tsv_fields ,  delimiter=' ')
    output.writeheader()
        
    sample_ids = data.keys()    
    common_mutations = set.intersection(*[set(data[sample]['mutation_id'][item] for item in range(num)) for sample in data.keys()]) 

    label = labelled_mutations(data, num)

    for mutation in common_mutations:
        output.writerow({'mutation_id': mutation, 'label':label[mutation]})
                



def write_config_file(args):
    config_dic = {}
    config_dic['base_measure_params'] = {}
    config_dic['base_measure_params']['alpha'] = 1
    config_dic['base_measure_params']['beta'] = 1
    
    config_dic['beta_binomial_precision_params'] = {}
    config_dic['beta_binomial_precision_params']['prior'] = {}
    config_dic['beta_binomial_precision_params']['prior']['rate'] = 1
    config_dic['beta_binomial_precision_params']['prior']['shape'] = 100
    config_dic['beta_binomial_precision_params']['proposal'] = {}
    config_dic['beta_binomial_precision_params']['proposal']['precision'] = args.precision
    config_dic['beta_binomial_precision_params']['value'] = args.precision
    config_dic['concentration'] = {}
    config_dic['concentration']['prior'] = {}
    config_dic['concentration']['prior']['rate'] = 0.001
    config_dic['concentration']['prior']['shape'] = 1
    config_dic['concentration']['value'] = 1
    config_dic['density'] = 'pyclone_beta_binomial'
    config_dic['num_iters'] = 1
    config_dic['samples'] = {}
    for num in range(args.sample_num):
        sample_name = "S" + str(num)
        config_dic['samples'][sample_name] = {}
        config_dic['samples'][sample_name]['number'] = args.sample_num    
        config_dic['samples'][sample_name]['error_rate'] = args.error_rate
        config_dic['samples'][sample_name]['mutations_file'] = 'yaml/' + args.sample_name + '/' + sample_name + '.yaml'
        config_dic['samples'][sample_name]['flat_cluster_files'] = 'tmp/' + args.sample_name + '/' + sample_name + '_cluster2Phi.tsv'
        config_dic['samples'][sample_name]['tumour_content'] = {}
        config_dic['samples'][sample_name]['tumour_content']['value'] = args.tumour_content

    
    config_dic['trace_dir'] = 'tmp/' + args.sample_name
    config_dic['working_dir'] = args.out_dir   
    write_dic_yaml(config_dic, args.out_dir, args.sample_name)


def write_inputs(args):
    import os
    inputDir = os.path.join(args.out_dir, "config")
    if not os.path.exists(inputDir):
        os.makedirs(inputDir)
    inputFile = os.path.join(inputDir, args.sample_name + "_inputs.txt")
    with(open(inputFile,'w')) as f:
            for a in args.__dict__:
                f.write(a + " : " + str(args.__dict__[a]) + "\n")
                
    
def write_info_file(args, phi, tau, precision):
    import os
    infodir = os.path.join(args.out_dir, "tmp", args.sample_name)
    if not os.path.exists(infodir):
        os.makedirs(infodir)
    for sample in phi.keys():
        infofile = os.path.join(infodir, sample + "_info.txt")
        with(open(infofile,'w')) as f:
            f.write("phi = ")
            for item, t in zip(phi, tau): 
                f.write("(%s,%s) " %(item, t))
            f.write("\n")
            f.write("precision = ")
            f.write("%s\n" % precision)
        
        
## note that this currently outputs the normal cluster, too. Need to manually remove -- hack for now. 
def write_cluster2Phi(args, phi, tau):
    import os
    infodir = os.path.join(args.out_dir, "tmp", args.sample_name)
    if not os.path.exists(infodir):
        os.makedirs(infodir)
    for sample in phi.keys():
        
        clusterFile = os.path.join(infodir, sample + "_cluster2Phi.tsv")
        delimiter = " "
        with(open(clusterFile,'w')) as f:
            f.write("mcmc" + delimiter + "prior" + delimiter + "phi" + "\n")
            for phiElement, tauElement in zip(phi[sample], tau): 
                f.write(("1" + delimiter + "%s" + delimiter + "%s" + "\n") %(tauElement, phiElement))

def write_dic_yaml(dic, out_dir, name):
    import os
    odir = os.path.join(out_dir, 'config')
    if not os.path.exists(odir):
        os.makedirs(odir)
    ofile = os.path.join(odir,  name + '.yaml')
    with open(ofile, 'w') as outfile:
        outfile.write(yaml.dump(dic, default_flow_style=False ))

        
        
    
if __name__=='__main__':
    parser = argparse.ArgumentParser(prog='MuClone')
    parser.add_argument('--hg_dir', help = '''the reference genome directory''')
    parser.add_argument('--pos_num', type=int, help='''the number of positions''')
    parser.add_argument('--cluster_num', type=int, help='''the number of clusters''')
    parser.add_argument('--sample_num', type=int, default = 1, help='''the number of samples''')
    parser.add_argument('--tumour_content', type=float, help='''tumour_content''')
    parser.add_argument('--depth', type=int, help='''avarage depth of the data''')
    parser.add_argument('--max_copy', type=int, help='''max copy number''')
    parser.add_argument('--wildtype_phi', type=float, default=0, help='''mu for normal cluster''')
    parser.add_argument('--max_wildtype_number', type=int, help='''number of normal position in simulated data''')
    parser.add_argument('--out_dir', help='''The directory of the output file''')
    parser.add_argument('--precision', type=int, default=10000, help='''precision parameter for beta-binomial''')
    parser.add_argument('--sample_name', default = 'Sample', help = '''sample_name''')
    parser.add_argument('--error_rate', type=float, help='''error rate for accounting for sequencing error''')
    parser.add_argument('--seed', type=int, default = 1)
    args = parser.parse_args()
    main(args)    