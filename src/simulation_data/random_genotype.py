'''
Created on Dec 5, 2014

@author: fdorri
'''

from random import randint
from numpy import random
import numpy as np
import math

def random_copy_number(rand, number, max_copy):
    c_tot = [rand.randint(1,int(max_copy)) for i in range(int(number))] 
    cc = [rand.randint(0,j) for j in c_tot]
    c_major = [max(i, j-i) for (i,j) in zip(cc, c_tot)]
    c_minor = [i-j for (i,j) in zip(c_tot, c_major)]
    
    return c_tot, c_major, c_minor

def get_phi(rand, cluster_number, wildtype_phi, error_rate):
    #phi have  error_rate*2 distance from 0 and 1.
    phi = [1]
#    beg = 1 - error_rate*2*2 
#    delta = beg / (cluster_number - 1)
#    low_bound =  error_rate*2
#    high_bound = low_bound + delta
#    for i in range(cluster_number - 1):
#        phi.append(rand.uniform(low=low_bound, high=high_bound, size=1))
#        low_bound = high_bound
#        high_bound = high_bound + delta

    for i in range(cluster_number - 1):
        val = rand.uniform(low=error_rate, high=1 - error_rate*2, size=1) * random.randint(2, size=1)[0]
        phi.append( val+ error_rate )
        #print phi

    phi = np.append(phi, wildtype_phi)

    return phi

def get_phi_tree(rand, num_sample, num_cluster, wildtype_phi,  phi_th, error_rate):
    clusters = {}

    clusters['1'] = {}
    clusters['1']['children'] = []
    clusters['1']['parent'] = None
    clusters['1']['phi'] = [1 for i in range(num_sample)]
    clusters['1']['label'] = [1 for i in range(num_sample)]
    recent_clusters = [1]
    # make a tree
    rc = num_cluster - 1
    last_cluster = 1

    while last_cluster < num_cluster:
        for cluster in recent_clusters:
            #print "cluster = " + str(cluster)
            xx = rand.randint(1, 3)
            if xx == 2:
                if last_cluster < num_cluster - 1:
                    last_cluster += 1
                    clusters[str(last_cluster)] = {}
                    clusters[str(cluster)]['children'].append(str(last_cluster))
                    clusters[str(last_cluster)]['children'] = []
                    clusters[str(last_cluster)]['phi'] = []
                    clusters[str(last_cluster)]['label'] = []
                    clusters[str(last_cluster)]['parent'] = str(cluster)
                    recent_clusters.remove(cluster)
                    recent_clusters.append(last_cluster)
                    last_cluster += 1
                    clusters[str(last_cluster)] = {}
                    clusters[str(cluster)]['children'].append(str(last_cluster))
                    clusters[str(last_cluster)]['children'] = []
                    clusters[str(last_cluster)]['phi'] = []
                    clusters[str(last_cluster)]['label'] = []
                    clusters[str(last_cluster)]['parent'] = str(cluster)
                    recent_clusters.append(last_cluster)
                else:
                    xx = 1

            if xx == 1:
                last_cluster += 1
                clusters[str(last_cluster)] = {}
                clusters[str(cluster)]['children'].append(str(last_cluster))
                clusters[str(last_cluster)]['children'] = []
                clusters[str(last_cluster)]['phi'] = []
                clusters[str(last_cluster)]['label'] = []
                clusters[str(last_cluster)]['parent'] = str(cluster)
                recent_clusters.remove(cluster)
                recent_clusters.append(last_cluster)
            for j in range(num_sample):
                parent_phi = clusters[str(cluster)]['phi'][j]

                for ch in clusters[str(cluster)]['children']:
                    v = rand.uniform(low=0, high=parent_phi, size=1)
                    #v = rand.uniform(low=error_rate*2, high=parent_phi, size=1)
                    clusters[ch]['phi'].append(v[0])
                    if v[0] >= phi_th:
                        clusters[ch]['label'].append(1)
                    else:
                        clusters[ch]['label'].append(0)

                    parent_phi -= v[0]
            if last_cluster == num_cluster:
                break

    for cluster in clusters.keys():
        if cluster != '1':
            idx = np.argmax(np.array(clusters[str(cluster)]['phi']))
            max_val = clusters[str(cluster)]['phi'][idx]
            for jj in range(len(clusters[str(cluster)]['phi'])):
                coin = random.randint(2, size=1)[0]
                clusters[str(cluster)]['phi'][jj] = clusters[str(cluster)]['phi'][jj] * coin
                clusters[str(cluster)]['label'][jj] = clusters[str(cluster)]['label'][jj] * coin
            #clusters[str(cluster)]['phi'] = [x * random.randint(2, size=1)[0] for x in
            #                                 clusters[str(cluster)]['phi']]

            clusters[str(cluster)]['phi'][idx] = max_val
            clusters[str(cluster)]['label'][idx] = 1

    sample_phi = {}
    sample_label = {}
    for j in range(num_sample):
        sample_phi[j] = []
        sample_label[j] =[]
        for c in clusters.keys():
            sample_phi[j].append(clusters[c]['phi'][j])
            sample_label[j].append(clusters[c]['label'][j])

        #sample_phi[j].append(wildtype_phi)
        sample_phi[j] = np.append(sample_phi[j], wildtype_phi)
        #sample_label[j].append(0)
        sample_label[j] = np.append(sample_label[j], 0)

    return sample_phi, sample_label


def get_phi_insreted_wildtype(rand, cluster_number, wildtype_phi, error_rate):
    phi = [1]
    label = [1]
    
#     beg = 1 - error_rate*2*2 
#     delta = beg / (cluster_number - 1)
#     low_bound =  error_rate*2
#     high_bound = low_bound + delta
#     for i in range(cluster_number - 1):
#         val = rand.uniform(low=low_bound, high=high_bound, size=1) 
#         coin = random.randint(2, size=1)[0]
#         if coin == 0:
#             phi.append(0)
#             label.append(0)
#         else:
#             phi.append(val)
#             label.append(1)
#         low_bound = high_bound
#         high_bound = high_bound + delta

    for i in range(cluster_number - 1):
        val = rand.uniform(low=error_rate*2, high=1 , size=1) 
        coin =  random.randint(2, size=1)[0]
        if coin == 0:
            phi.append(0)
            label.append(0)
        else:
            phi.append(val)
            label.append(1)        

        

    phi = np.append(phi, wildtype_phi)
    label = np.append(label, 0)
    
#    wildtype_num = rand.randint(1, cluster_number-1)
#    for i in range(wildtype_num):
#        which_cluster = rand.randint(1, cluster_number - 1)
#        phi[which_cluster] = wildtype_phi
#        label[which_cluster] = 0

        
### need to add sample labels
    return phi, label



def random_cellular_info(rand, pos_number, cluster_number, wildtype_tau):

    tau = rand.uniform(low=0.05, high=1, size=int(cluster_number))
    sum_tau = sum(tau)
    _tau = [x /sum_tau for x in tau]
    #print _tau


    tau = [x * (1- wildtype_tau)/sum_tau for x in tau]
    tau = np.append(tau, wildtype_tau)
    
    count_check = [math.floor(x* pos_number) for x in tau]
    if sum(count_check) != pos_number:
        count_check[-1] = count_check[-1] + (pos_number - sum(count_check) )
        
    tau = [float(x)/pos_number for x in count_check]


    return tau, count_check

def get_depth(rand, pos_number, depth):   
    return rand.poisson(int(depth), size= int(pos_number))


def get_genotype(rand, c_tot, c_major, c_minor, pos_id, eps):
    
    if pos_id == False:
        cn_n = 2
        cn_r = 2
        cn_v = 0
        mu_n = eps
        mu_r = eps
        mu_v = eps
        
    if pos_id == True:
        index = rand.randint(1, 2)
        if index == 1:
            cn_n = 2
            cn_r = 2
            cn_v = c_tot
                    
            mu_n = eps
            mu_r = eps
                    
            c_index = rand.randint(1, 2)
            if (c_index == 1) or (c_minor == 0):
                mu_v = float(c_major) / c_tot 
            else:
                mu_v = float(c_minor) / c_tot
                        
        if index == 2:
            cn_n = 2
            cn_r = c_tot
            cn_v = c_tot
                    
            mu_n = 0
            mu_r = 0 
            mu_v = float(1) / c_tot 
            
    return cn_n, cn_r, cn_v, mu_n, mu_r, mu_v
