'''
ref : http://genometoolbox.blogspot.com/2014/06/generate-random-genomic-positions.html

Created on Dec 5, 2014

@author: fdorri
'''

from operator import itemgetter

def make_random_pos(rand, sites, size, perms, hg_dir):
    positions =[]
    chr_len,chr_lst = chr_length(hg_dir)
    gaps = chr_gaps(hg_dir)
    a = make_random(rand, sites, size, chr_len, chr_lst, gaps)
    positions = make_pos_list(a, positions)       
    return positions
    

# Define functions
def chr_length(hg_dir):
    # Import chromosome cytobands
    cyto=open(hg_dir + "/cytoBand.txt").readlines()
    chr_len={}
    for i in range(len(cyto)):
        chr_raw, start, stop, id, g = cyto[i].strip().split()
        chrom = chr_raw.strip("chr")
        if chrom in chr_len:
            if int(stop)>chr_len[chrom]:
                chr_len[chrom]=int(stop)
        else:
            chr_len[chrom]=int(stop)
    del chr_len['Y']
    del chr_len['X']    
    chr_lst=[]
    
    for chrom in sorted([int(x) for x in chr_len]):
        chr_rep = [str(chrom)] * int(round(chr_len[str(chrom)]/10000000,0))
        for rep in chr_rep:
            chr_lst.append(rep)
    
    return chr_len, chr_lst


def chr_gaps(hg_dir):
    # Import chromosome gaps
    gap_raw=open(hg_dir + "/gap.txt").readlines()
    gaps={}
    for i in range(len(gap_raw)):
        bin, chr_raw, start, stop, ix, n, sz, type, br = gap_raw[i].strip().split()
        chrom = chr_raw.strip("chr")
        gap_region = [int(start), int(stop)]
        if chrom in gaps:
            gaps[chrom].append(gap_region)
        else:
            gaps[chrom] = [gap_region]
        
    return gaps
 
 
def rand_sites(rand, sites, size, chr_len, chr_lst, gaps):
    from random import choice,uniform
    count = 1
    # Choose chromosome and point on chromosome
    while count<=sites:
        chrom = rand.choice(chr_lst)
        point = int(rand.uniform(0 + size/2, chr_len[chrom] - size/2))
        
        # Exclude inaccessible regions
        include="T"
        for start, stop in gaps[chrom]:
            if start <= point<=stop:
                include = "F"
        
        # Return points in accessible regions
        if include=="T":
            yield count, int(chrom), point-size/2, point+size/2
            count+=1
            
            
def sort_coords(coords,cols=itemgetter(1,2)):
    unsorted=[]
    coord = iter(coords)
    item = next(coord,None)
    
    while item:
        unsorted.append(item)
        item = next(coord,None)
    
    sorted_regions = sorted(unsorted, key=cols)
    
    for i in range(len(sorted_regions)):
        yield sorted_regions[i]
             
def make_random(rand, sites, size, chr_len, chr_lst,gaps):
    a = rand_sites(rand, sites, size, chr_len, chr_lst, gaps)
    b = sort_coords(a)
    return b
           
def make_pos_list(gen_out, pos_list):
    for count, chrom, start, end in gen_out:
        pos_list.append("chr" + str(chrom) + ":" + str(start) )
    return pos_list
