'''
@author: fdorri, jewellsean
'''
from collections import OrderedDict
from utils import IO_Utils
from mcmc_processor import Mcmc_shaper
import os
import numpy as np
from distutils.command.config import config


class Pyclone_mcmc(object):

    def __init__(self, labels, precision, mcmc_iter, cellular_prevalence, mcmc_cluster2Phi):
        self.labels = labels
        self.precision = precision
        self.mcmc_iter = mcmc_iter
        self.cellular_prevalence = cellular_prevalence
        self.mcmc_cluster2Phi = mcmc_cluster2Phi
            
    @classmethod
    def fromConfig(cls, config):
        base_trace_path = os.path.join(config['working_dir'], config['trace_dir']) 
        
        labels_file = os.path.join(base_trace_path,'labels.tsv')
        labels = IO_Utils.read_mutation_trace(labels_file)
        
        precision_file = os.path.join(base_trace_path, 'precision.tsv')
        precision = IO_Utils.read_bare_trace_2OrderedDict(precision_file)
        
        cellular_prevalence = OrderedDict()
        mcmc_cluster2Phi = OrderedDict()
        for sample_id in config['samples']:
            cellular_prevalance_file = os.path.join(base_trace_path, 
                                                    sample_id + '.cellular_frequencies.tsv')
            cellular_prevalence[sample_id] = IO_Utils.read_mutation_trace(cellular_prevalance_file)
            mcmc_cluster2Phi[sample_id] = Mcmc_shaper.create_cluster2Phi(cellular_prevalence[sample_id], labels)
    
        mcmc_iter = np.max(cellular_prevalence[sample_id].mcmc) 
        cellular_prevalence = cellular_prevalence
        mcmc_cluster2Phi = mcmc_cluster2Phi
        
        return cls(labels, precision, mcmc_iter, cellular_prevalence, mcmc_cluster2Phi)
    
    @classmethod
    def fromFlat(cls, flat_files, precision, sample_name):
        mcmc_cluster2Phi = OrderedDict()
        for sample in flat_files.keys():
            mcmc_cluster2Phi[sample] = IO_Utils.read_flat_clusters(flat_files[sample])
        mcmc_iter = 1
        cellular_prevalence = None 
        labels = None
        precision_array = np.array([float(precision), float(precision)]) 
        return cls(labels, precision_array, mcmc_iter, cellular_prevalence, mcmc_cluster2Phi)
        
    
    @classmethod
    def fromFlatData(cls, data, precision):
        mcmc_cluster2Phi = OrderedDict()
        for sample in data.keys():
            mcmc_cluster2Phi[sample] = IO_Utils.flat_from_clusters(data[sample])
        mcmc_iter = 1
        cellular_prevalence = None 
        labels = None
        precision_array = np.array([float(precision), float(precision)]) 
        return cls(labels, precision_array, mcmc_iter, cellular_prevalence, mcmc_cluster2Phi)
    
    
    
    
        
