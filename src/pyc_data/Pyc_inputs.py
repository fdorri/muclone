'''
Created on Sep 11, 2014


Returns keys for sample_ids, common_mutations

@author: fdorri, jewellsean

'''

from collections import OrderedDict


from pyc_data import pyc_beta_binomial
from pyc_data.pyc_beta_binomial import _load_sample_data



import os
from utils.IO_Utils import _form_sample_data

class Pyclone_inputs():

    def __init__(self, sample_data, tumour_content, sample_ids, common_mutations, flat_cluster_files ):
        self.sample_data = sample_data
        self.tumour_content = tumour_content
        self.sample_ids = sample_ids
        self.common_mutations = common_mutations
        self.flat_cluster_files = flat_cluster_files
        
        
        
    @classmethod
    def fromConfig(cls, config):
        sample_data = OrderedDict()
        flat_cluster_files = OrderedDict()
        tumour_content = OrderedDict()
        
        for sample_id in config['samples']:
            file_name = config['samples'][sample_id]['mutations_file']   
            file_name = os.path.join(config['working_dir'], file_name)
           
            error_rate = config['samples'][sample_id]['error_rate']
            
            tumour_content[sample_id] = config['samples'][sample_id]['tumour_content']['value']
            flat_cluster_files[sample_id] = config['samples'][sample_id]['flat_cluster_files']
            flat_cluster_files[sample_id] = os.path.join(config['working_dir'],flat_cluster_files[sample_id])
            sample_data[sample_id] = _load_sample_data(file_name, error_rate, tumour_content[sample_id])
        
        common_mutations = set.intersection(*[set(x.keys()) for x in sample_data.values()])
        sample_ids = sample_data.keys()
        
        return cls(sample_data, tumour_content, sample_ids, common_mutations, flat_cluster_files)
    
    
    @classmethod
    def inPlace(cls, data, tumour_content, error_rate):
        sample_data = OrderedDict()
        for sample_name in data.keys():
            sample_data[sample_name] = _form_sample_data(data[sample_name], error_rate, tumour_content)
        common_mutations = set.intersection(*[set(x.keys()) for x in sample_data.values()])
        sample_ids = sample_data.keys()
        
        return cls(sample_data, tumour_content, sample_ids, common_mutations, None)
