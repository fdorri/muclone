'''
Created on Sep 11, 2014

@author: fdorri, jewellsean
'''

import numpy as np
import pandas as pd


def create_cluster2Phi(samples, labels):
    sample_melted = pd.melt(samples, id_vars='mcmc', var_name='mutation', value_name='phi')
    labels_melted = pd.melt(labels, id_vars='mcmc', var_name='mutation', value_name='cluster')

    merged = pd.merge(sample_melted, labels_melted, on=['mcmc', 'mutation'])

    grouped = merged.groupby(['mcmc', 'cluster'])
    aggregate_mcmc = grouped.agg({'cluster':{'nElements':np.size}, 'phi':{'avg' : np.mean}})
    
    return aggregate_mcmc
 

def extract_mcmcIter(aggregate_mcmc, mcmcIter, prior):
    mcmc_slice = aggregate_mcmc.query(('mcmc ==' + str(mcmcIter)))
    
    d = {'mcmc' : mcmcIter, 'cluster' : -1, 'nElements' : prior, 'phi' : float(0)}
    normal_cluster = pd.DataFrame(d, index = ['mcmc'])

    normal_cluster_group = normal_cluster.groupby(['mcmc', 'cluster'])
    normal  = normal_cluster_group.agg({'cluster':{'nElements':np.size}, 'phi':{'avg' : np.mean}})

    mcmc_slice = mcmc_slice.append(normal)
    
    nRow = mcmc_slice.cluster.nElements.count()  
    
    prior_prob = mcmc_slice.cluster.nElements.copy(deep = True) 
    S = sum(prior_prob)
    k = (1 - prior) / (S - normal.cluster.nElements.values)
    
    for i in range(nRow):
        if (i != (nRow - 1)):
            prior_prob[i:i+1] = prior_prob[i:i+1] * k 
        else:
            prior_prob[i] = prior  

    phi = pd.DataFrame(mcmc_slice.phi.avg)

    return prior_prob, phi 



