'''
Created on Sep 10, 2014

@author: fdorri, jewellsean

'''
from collections import OrderedDict
from mcmc_processor import Mcmc_shaper

class Cluster_cellular_prevalence(object):

    cluster_prevalence = []    
    precision = []

    def __init__(self, sample_ids, sample_mcmc_obj, mcmcIter, precision, prior):
        
        cellular_prevalence = OrderedDict()
        self.prior = float(prior)
                
        for sample in sample_ids:
            cellular_prevalence[sample] = OrderedDict()
            prior_prob, phi = Mcmc_shaper.extract_mcmcIter(sample_mcmc_obj[sample], mcmcIter, self.prior)
            cluster_ids = []
            for index in prior_prob.index:
                cluster = index[1]
                cluster_ids.append(cluster)
                self.cluster_ids = cluster_ids
                prior_cluster = prior_prob[index]
                phi_cluster = phi.xs(cluster, level='cluster').iloc[0][0]
                cellular_prevalence[sample][cluster] = (float(phi_cluster), float(prior_cluster))
            
        self.cluster_prevalence = cellular_prevalence   
        self.precision = precision
                
                        
        
        
        
        
